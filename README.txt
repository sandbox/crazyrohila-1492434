This is a light module to create a content type "job_vacancy". It allows Applicants to apply for job vacancies. 

The main question is :-

A nice module "job_posting" is already there so why I need this?

So these are the answers for this question :-

1. "job_posting" module is good for a "monster.com" like site, Where Each job (node) is posted by different companies and different persons. This is big enough for a single company jobs.

2. "job_posting" module provide all settings in content creation page. You have to enter your company information for job in all content.

3. "job_posting" module doesn't allow anonymous user to apply for job.

4. And "job_vacancy" is light weight module, that provides all necessary settings in admin settings and you don't have to enter them in all contents. 

5. "job_vacancy" has a permission. Admin can set if he wants anonymous user to apply for jobs or not.
